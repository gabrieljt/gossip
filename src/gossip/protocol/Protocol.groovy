package gossip.protocol

import gossip.network.HostManager
import gossip.network.Host

class Protocol implements Runnable {

    int port
    int clientHeartbeat
    int clientThreadsLimit
    int serverThreadsLimit
    ProtocolServer server

    Protocol(int port, int clientHeartbeat, int clientThreadsLimit, int serverThreadsLimit) {
        this.port = port
        this.server = new ProtocolServer(port, serverThreadsLimit)
        this.clientHeartbeat = clientHeartbeat
        this.clientThreadsLimit = clientThreadsLimit
        this.serverThreadsLimit = serverThreadsLimit
    }

    @Override
    void run() {

        def serverThread = Thread.start { server.run() }
        def clientThread
        int hostIndex
        Random hostRandom = new Random(Thread.currentThread().id)
        Host host
        while (serverThread.alive) {
            while (StateManager.infective && HostManager.activeHostsCount > 0) {
                hostIndex = hostRandom.nextInt(HostManager.activeHostsCount)
                host = HostManager.activeHosts.get(hostIndex)
                clientThread = Thread.start { new ProtocolClient(host, port).run() }
                clientThread.join()
                sleep(clientHeartbeat*1000)
            }
        }
        serverThread.join()
    }

}

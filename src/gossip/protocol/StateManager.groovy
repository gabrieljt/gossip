package gossip.protocol

import gossip.logger.Log

class StateManager implements Runnable {

    static StateType state = StateType.SUSCEPTIBLE

    static synchronized void updateState(StateType state) {
        if (!this.state.equals(state)) {
            this.state = state
            Log.INFO("Estado atualizado para $state.")
        }
    }

    static synchronized boolean isSusceptible() {
        return state.equals(StateType.SUSCEPTIBLE)
    }

    static synchronized boolean isInfective() {
        return state.equals(StateType.INFECTIVE)
    }

    static synchronized boolean isRemoved() {
        return state.equals(StateType.REMOVED)
    }

    @Override
    void run() {
        int sleepTime = 1000
        while (true) {
            while(StateManager.isRemoved()) {
                sleepTime = new Random().nextInt(29)*1000 + 1000
                Log.INFO("Estado $StateType.REMOVED durante ${sleepTime/1000} segundo(s).")
                sleep(sleepTime)

                StateManager.updateState(StateType.SUSCEPTIBLE)
            }
        }
    }
}

public enum StateType {
    SUSCEPTIBLE("SUSCEPTIBLE"),
    INFECTIVE("INFECTIVE"),
    REMOVED("REMOVED")

    final String value

    StateType(String value) { this.value = value }

    public String toString() { this.value }

    String getKey() { name() }

}

package gossip.protocol

import gossip.logger.Log
import gossip.network.Host
import gossip.network.HostManager
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

class ProtocolServer implements Runnable {

    ServerSocket serverSocket
    Socket clientSocket
    int port
    ArrayList<Thread> serverThreads = []
    int serverThreadsLimit = 10

    ProtocolServer(int port, int serverThreadsLimit) {
        this.port = port
        this.serverThreadsLimit = serverThreadsLimit
        listen()
    }

    /*
    do {
        m = receive (&p);
        if(susceptible && newMsg(m))
            state=infective;

    } while (susceptible);
    */

    @Override
    void run() {

        def serverThread
        while (true) {
            if (accept()) {
                serverThread = new Thread(clientSocket.inetAddress.hostAddress)
                serverThread.start { new ClientHandler(clientSocket).run() }
                serverThreads.add(serverThread)
            }
        }
    }

    void listen() {
        try {
            serverSocket = new ServerSocket(port)
            Log.INFO("Escutando na porta $port")
        } catch (IOException e) {
            Log.ERROR("Erro ao escutar na porta $port")
            System.exit(1)
        }
    }

    boolean accept() {
        try {
            clientSocket = serverSocket.accept()
        } catch (IOException e) {
            return false
        }
        def serverThread = serverThreads.find { it.name.equals(clientSocket.inetAddress.hostAddress) }
        if (serverThread) {
            if (serverThread.alive) {
                Log.WARNING("Host $clientSocket.inetAddress.hostAddress enviando múltiplas requisições.")
                return false
            } else {
                serverThreads.remove(serverThread)
            }
        }
        if (serverThreads.size() >= serverThreadsLimit) {
            for (thread in serverThreads) {
                thread.join()
                serverThreads.remove(thread)
            }
        }
        return true
    }
}

class ClientHandler implements Runnable {

    Socket client
    Info message
    PrintWriter output
    BufferedReader input


    ClientHandler(Socket clientSocket) {
        this.client = clientSocket
        this.output = new PrintWriter(client.getOutputStream(), true)
        this.input = new BufferedReader(new InputStreamReader(client.getInputStream()))
    }

    @Override
    void run() {
        message = receive()
        if (!InfoManager.isOldInfo(message) && StateManager.isSusceptible())
            StateManager.updateState(StateType.INFECTIVE)
    }

    synchronized Info receive() {
        Info fromClient, fromServer
        String fromClientString
         try {
        //Identificando cliente...
        def identifyClientThread = Thread.start { identifyClient(client) }
        //Recebendo mensagem
        fromClientString = input.readLine()
        //Verificando mensagem
        if (fromClientString) {
            if (fromClientString.equals(InfoManager.noNewInfo))
                fromClient = null
            else {
                fromClient = (Info) new JsonSlurper().parseText(fromClientString)
            }
        } else fromClient = null
        //Enviando mensagem, necessário verificar
        fromServer = InfoManager.prepareMessage()
        fromServer ? output.println(new JsonBuilder(fromServer)) : output.println(InfoManager.noNewInfo)
        //OK, liberando recursos
        output.close()
        input.close()
        client.close()
        identifyClientThread.join()
        Log.INFO("Informação recebida: $fromClient")
        fromServer ? Log.INFO("Informação enviada: $fromServer") : Log.INFO("Informação enviada: $InfoManager.noNewInfo")
         } catch (Exception e) {
             fromClient = null
             Log.WARNING("Erro Servidor: $e.message")
         }
        return fromClient
    }

    Host identifyClient(Socket client) {
        Host host = HostManager.findHostByAddress(client.inetAddress.hostAddress)
        host ? Log.INFO("Host conectou: $host") : Log.INFO("Host desconhecido conectou: $client.inetAddress.hostAddress")
        return host
    }
}

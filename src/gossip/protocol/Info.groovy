package gossip.protocol

import gossip.network.HostManager

class Info {

    String owner
    String data
    String dateCreated
    boolean isNew = true

    Info() {}

    Info(String data) {
        this.owner = HostManager.host.name
        if (data.length() > InfoManager.infoMaxSize) data = data.substring(0, InfoManager.infoMaxSize)
        this.data = data
        dateCreated = new Date().toTimestamp().toString()
    }

    String toString() {
        return "[Dados: $data, Criado por: $owner, Data: $dateCreated]"
    }

}

package gossip.protocol

import gossip.logger.Log
import gossip.network.HostManager
import java.sql.Timestamp

class InfoManagerException extends RuntimeException {
    String message
    def info
}

class InfoManager implements Runnable {

    boolean userInfo = true
    static int infoMaxSize
    static ArrayList<Info> infoList = []
    static final String noNewInfo = "[Sem novidades...]"

    InfoManager(boolean userInfo, int infoMaxSize) {
        this.userInfo = userInfo
        this.infoMaxSize = infoMaxSize
    }

    @Override
    void run() {
        def infoThread
        while (true) {
            while (StateManager.susceptible && HostManager.activeHostsCount > 0) {
                if (userInfo)
                    infoThread = Thread.start { userInfo() }
                else
                    infoThread = Thread.start { autoInfo() }
                infoThread.join(10000)
            }
        }
    }

    static synchronized boolean isOldInfo(Info receivedInfo) {
        if (receivedInfo) {
            Info info = InfoManager.findInfo(receivedInfo)
            if (info) {
                Log.INFO("Informação recebida é conhecida.")
                if (info.isNew) InfoManager.updateToOldInfo(info)
                return true
            }
            else {
                Log.INFO("Informação recebida é nova.")
                InfoManager.addInfo(receivedInfo)
                return false
            }
        } else return true
    }

    static synchronized Info prepareMessage() {
        Info info
        if (InfoManager.newInfoListCount > 0) {
            int infoIndex = new Random().nextInt(InfoManager.newInfoListCount)
            info = InfoManager.newInfoList.get(infoIndex)
        } else info = null

        return info
    }

    static synchronized Info updateToOldInfo(Info info) {
        info.isNew = false
        return info
    }

    static synchronized Info addInfo(Info info) {
        if(!this.infoList.add(info))
            throw new InfoManagerException(message: "Erro ao adicionar informação: $info", info: info)
        Log.INFO("Informação adicionada: $info")
        return info
    }

    static synchronized Info findInfo(Info info) {
        return infoList.find { it.data.equals(info.data) && it.owner.equals(info.owner) &&
                Timestamp.valueOf(it.dateCreated).equals(Timestamp.valueOf(info.dateCreated)) }
    }

    static synchronized ArrayList<Info> getInfoList() {
        return infoList
    }

    static synchronized ArrayList<Info> getNewInfoList() {
        return infoList.findAll { it.isNew }
    }

    static synchronized ArrayList<Info> getOldInfoList() {
        return infoList.findAll { !it.isNew }
    }

    static synchronized int getInfoListCount() {
        return getInfoList().size()
    }

    static synchronized int getNewInfoListCount() {
        return getNewInfoList().size()
    }

    static synchronized int getOldInfoListCount() {
        return getOldInfoList().size()
    }

    void autoInfo() {
        Info info
        try {
            info = new Info(UUID.randomUUID().toString())
            addInfo(info)
            StateManager.updateState(StateType.INFECTIVE)
        } catch (InfoManagerException e) {
            Log.ERROR(e.message)
        }
    }

    void userInfo() {
        String userInfo = ""
        Scanner scanner = new Scanner(System.in)
        Info info
        try {
            Log.INFO("Escreva mensagem para propagar: ")
            while (!userInfo)
                userInfo = scanner.nextLine()
            info = new Info(userInfo)
            addInfo(info)
            StateManager.updateState(StateType.INFECTIVE)
        } catch (InfoManagerException e) {
            Log.ERROR(e.message)
        }

        addShutdownHook {
            scanner.close()
        }
    }
}

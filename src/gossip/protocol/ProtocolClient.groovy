package gossip.protocol

import gossip.logger.Log
import gossip.network.Host
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

class ProtocolClient implements Runnable {

    Host host
    Socket socket
    PrintWriter output
    BufferedReader input
    int port
    Info info

    ProtocolClient(Host host, int port) {
        this.host = host
        this.port = port
    }

    /**
     do {
     usleep(..X..)
     p = selectPeer()
     m = prepareMsg()
     r = send (m, p)
     if(oldMsg(r))
     state=removed
     } while (state==infective)
     */

    @Override
    void run() {
        if (selectPeer()) {
            Log.INFO("Conectando ao host: $host")
            info = InfoManager.prepareMessage()
            if (info) {
                info = send(info)
                if (InfoManager.isOldInfo(info))
                    StateManager.updateState(StateType.REMOVED)
            }
        }
    }

    Socket selectPeer() {
        try {
            socket = new Socket(host.address, port)
            output = new PrintWriter(socket.getOutputStream(), true)
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        } catch (UnknownHostException e) {
            socket = null
        } catch (IOException e) {
            socket = null
        }
        return socket
    }

    synchronized Info send(Info fromClient) {
        Info fromServer
        String fromServerString
        try {
            //Enviando mensagem
            output.println(new JsonBuilder(fromClient))
            //Recebendo mensagem
            fromServerString = input.readLine()
            //Verificando mensagem
            if (fromServerString) {
                if (fromServerString.equals(InfoManager.noNewInfo))
                    fromServer = null
                else {
                    fromServer = (Info) new JsonSlurper().parseText(fromServerString)
                }
            } else fromServer = null
            //OK, liberando recursos
            output.close()
            input.close()
            socket.close()
            Log.INFO("Informação enviada: $fromClient")
            fromServer ? Log.INFO("Informação recebida: $fromServer") : Log.INFO("Informação recebida: $fromServerString")
        } catch (Exception e) {
            fromServer = null
            Log.WARNING("Erro Cliente: $e.message")
        }

        return fromServer
    }
}

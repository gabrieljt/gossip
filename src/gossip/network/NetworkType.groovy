package gossip.network

public enum NetworkType {
    LAN("LAN")

    final String value

    NetworkType(String value) { this.value = value }

    public String toString() { this.value }

    String getKey() { name() }
}
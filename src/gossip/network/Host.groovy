package gossip.network

class Host {

    String address
    String name
    String date
    boolean isOnline = true

    Host() {}

    Host(String address, String name) {
        this.address = address
        this.name = name
        this.date = new Date().toTimestamp().toString()
    }

    String toString() {
        return "[Nome: $name, Endereço IP: $address, Data: $date]"
    }

}

package gossip.network

class Multicast implements Runnable {

    InetAddress group
    MulticastSocket multicastSocket
    int port
    int heartbeat
    def sendMessage
    def receiveMessage
    DatagramPacket sendPacket
    DatagramPacket receivePacket
    int bufferSize
    byte[] buffer

    Multicast(String group, int port, int heartbeat, int bufferSize, int timeToLive) {
        this.group = InetAddress.getByName(group)
        this.multicastSocket = new MulticastSocket(port)
        this.multicastSocket.timeToLive = timeToLive
        this.port = port
        this.heartbeat = heartbeat
        this.bufferSize = bufferSize
        this.joinGroup()
    }

    @Override
    void run() {
        sendPacket = new DatagramPacket(sendMessage.toString().getBytes(), sendMessage.toString().length(), group, port)
        multicastSocket.send(sendPacket)
        buffer = new byte[bufferSize]
        receivePacket = new DatagramPacket(buffer, buffer.length)
        multicastSocket.receive(receivePacket)
        receiveMessage = buffer
    }

    void joinGroup() { multicastSocket.joinGroup(group) }

    void leaveGroup() { multicastSocket.leaveGroup(group) }

    void close() { multicastSocket.close() }
}

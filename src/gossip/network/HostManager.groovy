package gossip.network

import gossip.logger.Log
import groovy.json.JsonBuilder
import java.sql.Timestamp
import groovy.json.JsonSlurper
import groovy.json.JsonException

class HostManagerException extends RuntimeException {
    String message
    def host
}

class HostManager implements Runnable {

    NetworkType networkType
    String routerAddress
    String multicastGroup
    int multicastPort
    int multicastHeartbeat
    int multicastBufferSize
    int multicastTimeToLive
    Multicast multicast

    static Host host //esse nó!
    static ArrayList<Host> hosts = [] //nós conhecidos

    HostManager(NetworkType networkType, String routerAddress, String multicastGroup, int multicastPort, int multicastHeartbeat, int multicastBufferSize, int multicastTimeToLive) {
        this.networkType = networkType
        this.multicastGroup = multicastGroup
        this.multicastPort = multicastPort
        this.multicastHeartbeat = multicastHeartbeat
        this.multicastBufferSize = multicastBufferSize
        this.multicastTimeToLive = multicastTimeToLive
        this.routerAddress = routerAddress

        String hostAddress = ""
        String hostName = InetAddress.localHost.hostName

        Log.INFO("Configurando Host...")
        switch (networkType) {
            case NetworkType.LAN:
                for (NetworkInterface networkInterface in NetworkInterface.networkInterfaces)
                    for (InetAddress inetAddress in networkInterface.inetAddresses)
                        if (inetAddress.hostAddress.startsWith("192.") || inetAddress.hostAddress.startsWith("10."))
                            hostAddress = inetAddress.hostAddress

                if (routerAddress) {
                    Socket s = new Socket(routerAddress, 80);
                    host = new Host(s.localAddress.hostAddress, s.localAddress.localHost.hostName)
                    s.close();
                } else if (hostAddress && hostName) {
                    host = new Host(hostAddress, hostName)
                } else {
                    Log.ERROR("Não foi possível definir o endereço na Rede Local.")
                    System.exit(1)
                }
        }
        addHost(host)
        multicast = new Multicast(multicastGroup, multicastPort, multicastHeartbeat, multicastBufferSize, multicastTimeToLive)
    }

    @Override
    void run() {
        addShutdownHook {
            sendGoodbye()
        }

        def discoverNetworkThread
        while (multicast) {
            discoverNetworkThread = Thread.start { discoverNetwork() }
            discoverNetworkThread.join()
            sleep(multicast.heartbeat*1000)
        }
    }

    void discoverNetwork() {
        def multicastThread
        multicast.sendMessage = new JsonBuilder(HostManager.hosts)
        multicastThread = Thread.start { multicast.run() }
        multicastThread.join()
        try {
            multicast.receiveMessage = new JsonSlurper().parseText(new String(multicast.buffer).trim())
            addOrUpdateHosts((ArrayList<Host>) multicast.receiveMessage)
        } catch (JsonException e) {
            Log.WARNING("Não foi possível interpretar mensagem recebida.")
        }
    }

    static synchronized Host addHost(Host host) {
        if (!hosts.add(host))
            throw new HostManagerException(message: "Erro ao adicionar host: $host", host: host)
        Log.INFO("Host adicionado: $host")
        return host
    }

    static synchronized Host removeHost(Host host) {
        if (!hosts.remove(host))
            throw new HostManagerException(message: "Erro ao remover host: $host", host: host)
        Log.INFO("Host removido: $host")
        return host
    }

    static synchronized Host updateHost(Host newHost) {
        Host host = findHost(newHost)
        host.date = newHost.date
        host.name = newHost.name
        Log.INFO("Host atualizado: $host")
        newHost.isOnline ? activateHost(host) : inactivateHost(host)
    }

    static synchronized Host activateHost(Host host) {
        host.isOnline = true
        Log.INFO("Host Online: $host")
        return host
    }

    static synchronized Host inactivateHost(Host host) {
        host.isOnline = false
        Log.INFO("Host Offline: $host")
        return host
    }

    static synchronized Host findHost(Host host) {
        return hosts.find { it.address.equals(host.address) }
    }

    static synchronized Host findHostByAddress(String address) {
        return hosts.find { it.address.equals(address) }
    }

    static synchronized ArrayList<Host> getActiveHosts() {
        return hosts.findAll { it.isOnline && !it.address.equals(host.address) } //desconsiderando este nó
    }

    static synchronized ArrayList<Host> getInactiveHosts() {
        return hosts.findAll { !it.isOnline }
    }

    static synchronized int getActiveHostsCount() {
        return getActiveHosts().size()
    }

    static synchronized int getInactiveHostsCount() {
        return getInactiveHosts().size()
    }

    void addOrUpdateHosts(ArrayList<Host> hosts) {
        Host activeHost
        for (Host host in hosts) {
            activeHost = HostManager.findHost(host)
            if (!activeHost && host.isOnline) {
                HostManager.addHost(host)
            } else if (Timestamp.valueOf(host?.date) > Timestamp.valueOf(activeHost?.date)) {
                HostManager.updateHost(host)
            }
        }
    }

    void sendGoodbye() {
        if (multicast) {
            Log.WARNING("Enviando mensagem de despedida... :/")
            host.date = new Date().toTimestamp().toString()
            host = inactivateHost(host)
            ArrayList<Host> goodbye = new ArrayList<Host>()
            goodbye.add(host)
            multicast.sendMessage = new JsonBuilder(goodbye)
            def multicastThread = Thread.start { multicast.run() }
            multicastThread.join()
            multicast.leaveGroup()
            multicast.close()
        }
    }
}

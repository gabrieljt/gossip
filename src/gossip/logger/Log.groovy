package gossip.logger

class Log {

    static boolean enabled = true

    static boolean enableLog() {
        return enabled = true
    }

    static boolean disableLog() {
        return enabled = false
    }

    static void INFO(String message) {
        String infoMessage = "\n---${new Date().toTimestamp().toString()}---\n$LogType.INFO $message\n"
        if (enabled) print(infoMessage)
    }

    static void WARNING(String message) {
        String warningMessage = "\n---${new Date().toTimestamp().toString()}---\n$LogType.WARNING $message\n"
        print(warningMessage)
    }

    static void ERROR(String message) {
        String errorMessage = "\n---${new Date().toTimestamp().toString()}---\n$LogType.WARNING $message\n"
        print(errorMessage)
    }
}

public enum LogType {
    INFO("[INFO]"),
    WARNING("[WARNING]"),
    ERROR("[ERROR]")

    final String value

    LogType(String value) { this.value = value }

    public String toString() { this.value }

    String getKey() { name() }
}

package gossip.client

import gossip.network.HostManager
import gossip.network.NetworkType
import gossip.logger.Log
import gossip.protocol.StateManager
import gossip.protocol.Protocol
import gossip.protocol.InfoManager

class Gossip {

    static boolean logEnabled = true
    static NetworkType networkType = NetworkType.LAN
    static String routerAddress = ""
    static int protocolPort = 22222
    static int protocolClientHeartbeat = 5
    static int protocolClientThreadsLimit = 10
    static int protocolServerThreadsLimit = 10
    static int multicastPort = 33333
    static String multicastGroup = "224.0.0.1" //all hosts
    static int multicastHeartbeat = 10
    static int multicastBufferSize = 1024
    static int multicastTimeToLive = 1
    static boolean userInfo = false
    static int infoMaxSize = 50

    static def commandLineConfig

    static void main(args) {

        commandLineConfig = new CliBuilder(usage: 'java -jar gossip-<version>.jar [opções]' )
        commandLineConfig.h(longOpt: 'help', 'informações de uso', required: false)
        commandLineConfig.ra(longOpt: 'router-address', 'endereço do roteador', required: false, args: 1)
        commandLineConfig.l(longOpt: 'log', 'log desabilitado', required: false)
        commandLineConfig.pp(longOpt: 'protocol-port', 'protocolo: porta', required: false, args: 1)
        commandLineConfig.pch(longOpt: 'protocol-client-heartbeat', 'protocolo: heartbeat cliente', required: false, args: 1)
        commandLineConfig.pct(longOpt: 'protocol-client-threads-limit', 'protocolo: limite threads cliente', required: false, args: 1)
        commandLineConfig.pst(longOpt: 'protocol-server-threads-limit', 'protocolo: limite threads servidor', required: false, args: 1)
        commandLineConfig.mp(longOpt: 'multicast-port', 'multicast: porta', required: false, args: 1)
        commandLineConfig.mg(longOpt: 'multicast-group', 'multicast: grupo', required: false, args: 1)
        commandLineConfig.mh(longOpt: 'multicast-heartbeat', 'multicast: heartbeat', required: false, args: 1)
        commandLineConfig.mb(longOpt: 'multicast-buffer-size', 'multicast: tamanho buffer', required: false, args: 1)
        commandLineConfig.mttl(longOpt: 'multicast-ttl', 'multicast: time to live', required: false, args: 1)
        commandLineConfig.iu(longOpt: 'info-user', 'informações: pelo usuário', required: false)
        commandLineConfig.isz(longOpt: 'info-size', 'informações: tamanho máximo', required: false, args: 1)

        OptionAccessor opt = commandLineConfig.parse(args)
        if (!opt) {
            return
        }
        // print usage if -h, --help
        if (opt.h) {
            commandLineConfig.usage()
            return
        }

        if (opt.ra) routerAddress = opt.ra
        if (opt.l) logEnabled = false
        if (opt.pp) protocolPort = opt.pp
        if (opt.pch) protocolClientHeartbeat = opt.pch
        if (opt.pct) protocolClientThreadsLimit = opt.pct
        if (opt.pst) protocolServerThreadsLimit = opt.pst
        if (opt.mp) multicastPort = opt.mp
        if (opt.mg) multicastGroup = opt.mg
        if (opt.mh) multicastHeartbeat = opt.mh
        if (opt.mb) multicastBufferSize = opt.mb
        if (opt.mttl) multicastTimeToLive = opt.mttl
        if (opt.iu) userInfo = true
        if (opt.isz) infoMaxSize = opt.isz

        init()
    }

    static void init() {

        addShutdownHook {
            Log.WARNING("Encerrando Gossip Protocol...")
        }

        if (logEnabled)
            Log.enableLog()
        else
            Log.disableLog()
        Log.INFO("Inicializando Gossip Protocol...")
        HostManager hostManager = new HostManager(networkType, routerAddress, multicastGroup, multicastPort, multicastHeartbeat, multicastBufferSize, multicastTimeToLive)
        StateManager stateManager = new StateManager()
        Protocol protocol = new Protocol(protocolPort, protocolClientHeartbeat, protocolClientThreadsLimit, protocolServerThreadsLimit)
        InfoManager infoManager = new InfoManager(userInfo, infoMaxSize)

        Thread.start { hostManager.run() }
        Thread.start { stateManager.run() }
        Thread.start { protocol.run() }
        Thread.start { infoManager.run() }
    }

}
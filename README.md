Gossip Protocol

Gabriel J. Trabasso, Thiago N. Sanchez

Observações:
    É recomendado o Oracle JRE 7 para rodar a aplicação: 
        http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html

    Ainda não foi implementado a captura de parâmetros da linha de comando. Os parâmetros são auto-explicativos e se encontram em Gossip.groovy; para compilar o programa é necessário ter o Apache Ant instalado e executar o comando 'ant' na raíz do projeto.
        http://ant.apache.org/bindownload.cgi
        ou 'apt-get install ant' para SO Debian/Ubuntu

Índice:
    1- Descobrimento/Gerenciamento da Rede Local
    
    2- Gossip Protocol
    
    3- Informações
    
    4- Marshalling e Unmarshalling - Json
    
    5- O que foi feito, o que deveria ser feito (para melhorar qualidade) e o que deve ser feito (para atender a 
        proposta do protocolo Gossip)
    
    6- Resultados

-----

O programa possui quatro threads principais: descobrimento e gerenciamento da rede, gerenciamento de estado (SUSCEPTIBLE, INFECTED, REMOVED) do nó, protocolo gossip (cliente e servidor) e gerenciamento de informações.

    //Gossip.groovy
    ...
    Thread.start { hostManager.run() }
    Thread.start { stateManager.run() }
    Thread.start { protocol.run() }
    Thread.start { infoManager.run() }
    ...

Rede Local:

1- Descobrimento/Gerenciamento da Rede Local:
	
    Ao iniciar a aplicação, um objeto Host correspondente a máquina em questão é criado e adicionado à lista de hosts ativos.
    Além disso, um objeto Multicast também é criado, que junta-se ao grupo "all hosts" em seu construtor.

    //Host.groovy
    class Host {

        String address
        String name
        String date //timestamp da última atualização deste host
        boolean isOnline = true

        ...
    }

    //HostManager.groovy; construtor
    ...
    host = new Host(...)
    addHost(host)
    multicast = new Multicast(multicastGroup, multicastPort, multicastHeartbeat, multicastBufferSize, multicastTimeToLive)
    ...

    //Multicast.groovy; construtor    
    Multicast(String group, int port, int heartbeat, int bufferSize, int timeToLive) {
        this.group = InetAddress.getByName(group)
        this.multicastSocket = new MulticastSocket(port)
        ...
        this.joinGroup()
    }

    O descobrimento/gerenciamento da rede local é feito através de Multicast.
    Uma thread é responsável por enviar todos os hosts conhecidos pelo nó em questão através de multicast a cada 'hearbeat' segundos, e também por receber essas mensagens dos outros nós. Os hosts que não estão presentes na lista de hosts e estão online são adicionados. Os que estão com um timestamp mais recente são atualizados.

    //HostManager.groovy; thread responsável por enviar e receber multicast, além de atualizar lista de membros //ativos
    while (multicast) {
        discoverNetworkThread = Thread.start { discoverNetwork() }
        discoverNetworkThread.join()
        sleep(multicast.heartbeat*1000)
    }

    void discoverNetwork() {
        def multicastThread
        //envio da mensagem
        multicast.sendMessage = ...
        multicastThread = Thread.start { multicast.run() }
        multicastThread.join()
        //recebimento da mensagem
        ...
        addOrUpdateHosts((ArrayList<Host>) multicast.receiveMessage)
        //atualização da lista de hosts, adiciona host se não existir na lista (e está online) ou atualiza se o //timestamp for mais recente. Host correspondente a máquina em questão é desconsiderado.
        ...
    }

	Existe também um 'shutdown hook' responsável por enviar uma mensagem de 'despedida' aos demais membros, além de liberar os recursos necessários. O status 'isOnline' do Host é atualizado para 'false', e o timestamp também é atualizado, fazendo com que os demais hosts atualizem esse host como offline em suas listas e passem a desconsiderar este host. Se ficar online novamente, o timestamp será mais recente e será atualizado como 'online' novamente.

    //HostManager.groovy; mensagem de despedida
    addShutdownHook {
    	sendGoodbye()
    }

    void sendGoodbye() {
        if (multicast) {
            ...
            host.date = new Date().toTimestamp().toString() //atualização do timestamp
            host = inactivateHost(host) //isOnline = false
            ...
            goodbye.add(host)
            multicast.sendMessage = ...
            def multicastThread = Thread.start { multicast.run() }
            multicastThread.join()
            multicast.leaveGroup()
            multicast.close()
        }
    }
        
    Desta forma, a lista de hosts se mantém atualizada automaticamente. Um nó ao entrar no grupo avisa aos demais, os que já estão no grupo avisam todos os que conhecem aos demais e ao encerrar a aplicação, uma mensagem de despedida é enviada.

    Maiores detalhes, conferir arquivos Host.groovy, HostManager.groovy e Multicast.groovy.

    Dúvidas? Críticas? Sugestões?

2- Gossip Protocol

	Os nós podem propagar e receber informações conforme a lógica do Gossip Protocol, que não será muito discutida aqui. 
    Essas informações são propagadas e recebidas através do protocolo de comunicação TCP/IP. A classe ProtocolClient.groovy é responsável por propagar as informações, e a classe ProtocolServer.groovy por receber as informações. Duas threads gerenciam esses objetos.

    //Protocol.groovy
    ...
    def serverThread = Thread.start { server.run() }
    ...
    while (serverThread.alive) {
        while (StateManager.infective && HostManager.activeHostsCount > 0) { //propaga informações novas enquanto 
                                                                             //estiver infected
            ... //seleção aleatória do host para propagar mensagem
            clientThread = Thread.start { new ProtocolClient(host, port).run() }
            clientThread.join()
            sleep(clientHeartbeat*1000)
        }
    }
    serverThread.join()
    
    2.1- ProtocolServer.groovy

    O servidor sempre está apto a receber informações, e pode atender múltiplos clientes.

    //ProtocolServer.groovy
    while (true) {
        if (accept()) { //aceita cliente
            serverThread = new Thread(clientSocket.inetAddress.hostAddress)
            serverThread.start { new ClientHandler(clientSocket).run() } //nova thread para atender cliente
            serverThreads.add(serverThread)
        }
    }

    //ClientHandler; construtor para atender novo cliente 
    ClientHandler(Socket clientSocket) {
        this.client = clientSocket
        this.output = new PrintWriter(client.getOutputStream(), true)
        this.input = new BufferedReader(new InputStreamReader(client.getInputStream()))
    }

    //ClientHandler; lógica do Gossip Protocol    
    void run() {
        message = receive()
        if (!InfoManager.isOldInfo(message) && StateManager.isSusceptible()) 
            StateManager.updateState(StateType.INFECTIVE)
    }

    O servidor além de receber informações, também envia informação aleatória ao cliente que se conectou.
    Os recursos são liberados no método receive(), e o método accept() possui filtros para não aceitar múltiplas requisições de um mesmo cliente enquanto já houver uma thread processando uma requisição deste cliente. As threads são armazenadas em uma lista, que é gerenciada no método accept().

    2.2- ProtocolClient.groovy

    Enquanto estiver 'INFECTED', hosts são selecionados aleatoriamente da lista, assim como a informação que será enviada, a cada 'heartbeat' segundos, como foi mostrado acima em Protocol.groovy.

    //ProtocolClient.groovy; lógica do Gossip Protocol
    void run() {
        if (selectPeer()) {
            Log.INFO("Conectando ao host: $host")
            info = InfoManager.prepareMessage() //seleção aleatória da mensagem, apenas informações novas
                                                //são selecionadas
            if (info) {
                info = send(info)
                if (InfoManager.isOldInfo(info))
                    StateManager.updateState(StateType.REMOVED)
            }
        }
    }

    Maiores detalhes, conferir arquivos Protocol.groovy, ProtocolServer.groovy e ProtocolClient.groovy.

    Dúvidas? Críticas? Sugestões?
    
3- Informações

    As informações que são trocadas entre os nós possuem a seguinte estrutura:

    //Info.groovy
    class Info {

        String owner
        String data
        String dateCreated
        boolean isNew = true
        ...
    }

    Desta forma, é possível saber quem criou a informação, quando foi criada, e se é nova para o nó que recebeu tal informação. Um nó só propaga informações que são novas para ele, e assim que recebe uma informação que já esta na lista de informações, é marcada como conhecida.

    //InfoManager.groovy; atualizando informação como conhecida
    static synchronized Info updateToOldInfo(Info info) {
        info.isNew = false
        return info
    }

    //InfoManager.groovy; lista de informações conhecidas
    static synchronized ArrayList<Info> getNewInfoList() {
        return infoList.findAll { it.isNew }
    }

    Existem duas maneiras de criar informações: automática e através do usuário. Informações somente são criadas se o nó estiver suscetível, e são propagadas (uma de cada vez, aleatoriamente) enquanto estiver infectado, como foi mostrado em 2- Gossip Protocol.

    //InfoManager.groovy; criação de informações
    void run() {
        def infoThread
        while (true) {
            while (StateManager.susceptible && HostManager.activeHostsCount > 0) {
                if (userInfo) {
                    infoThread = Thread.start { userInfo() }
                    infoThread.join(10000)
                }
                else {
                    infoThread = Thread.start { autoInfo() }
                    infoThread.join()
                }
            }
        }
    }

    Maiores detalhes, conferir arquivos InfoManager.groovy e Info.groovy. Protocol.groovy, ProtocolClient.groovy e ProtocolServer.groovy também interagem bastante com as informações.

    Dúvidas? Críticas? Sugestões?

4- Marshalling e Unmarshalling - Json

    Os nós utilizam Json para trocar objetos entre si.
    Não foi criado ainda uma Classe específica para fazer o Marshalling e o Unmarshalling...

    //ProtocolClient.groovy
    Info send(Info fromClient) {
        ...
        //Enviando mensagem
        output.println(new JsonBuilder(fromClient)) //Marshal
        ...
        //Recebendo mensagem
        fromServerString = input.readLine()
        fromServer = (Info) new JsonSlurper().parseText(fromServerString) //Unmarshal
        ...
    }

    TODO: criar classe com métodos estáticos marshalAsJson(Object obj) e unmarshalAsJson(String str)

    Maiores detalhes, conferir arquivos HostManager.groovy e ProtocolClient.groovy

    Dúvidas? Críticas? Sugestões?

5- O que foi feito, o que deveria ser feito (para melhorar qualidade) e o que deve ser feito (para atender a proposta do protocolo Gossip)

    5.1- O que foi feito?

        5.1.1) Descobrimento de rede local. Auto-gerenciamento da rede. Estrutura para propagar e receber mensagens via Multicast.
                HostManager.groovy, Host.groovy, Multicast.groovy

        5.1.2) Estruturas para propagar e receber mensagens (de múltiplos clientes) via TCP/IP.
                ProtocolServer.groovy, ProtocolClient.groovy

        5.1.3) Implementação da lógica Gossip Protocol.
                Protocol.groovy, ProtocolServer.groovy, ProtocolClient.groovy, StateManager.groovy

        5.1.4) Estruturas para Host e Informação.
                Host.groovy, Info.groovy

        5.1.5) Marshalling e Unmarshalling de objetos utilizando Json.

        5.1.6) Threads para gerenciar descobrimento da rede, geração de informação, estado do nó de acordo com a lógica do protocolo Gossip e propagação e recebimento das informações.
                HostManager.run(), InfoManager.run(), StateManager.run(). Protocol.run()

        5.1.7) Proteção e sincronismo de threads.

        5.1.8) Preocupação (na medida do possível) em não repetir conhecimento (DRY - Don't Repeat Yourself), desacoplamento de classes, reuso de código, boas práticas de programação, códigos auto-explicativos.

    5.2- O que deveria ser feito (para melhorar qualidade)?

        5.2.1) TESTES!!! (JUnit).

        5.2.2) Captura de parâmetros de linha de comando.

        5.2.3) Um .jar para o pacote network. O descobrimento/gerenciamento da rede não possui dependências com o projeto principal e pode ser importado como biblioteca em qualquer projeto Java :D (resultado de 5.1.8).

        5.2.4) Revisar uso de 'synchronized' em alguns métodos.

        5.2.5) Classe para fazer Marshalling e Unmarshalling de objetos.

        5.2.6) Interpretador de comandos. (propagar informações, consultar informações, listar informações, topologias)

        5.2.7) Impressão dos resultados para arquivo.

        5.2.8) Simular rede local para testes com maior número de nós. Como? Máquinas Virtuais?

        5.2.9) Testar em diferentes redes.

    5.3- O que deve ser feito (para atender a proposta do protocolo Gossip)?

    Os itens aqui listados ainda não foram implementados por questão de tempo, e podem ser facilmente adaptados para o código atual. Acreditamos que é melhor consolidar os padrões de projeto antes de expandir as funcionalidades!

        5.3.1) Troca de Vistas Parciais

            Existe a possibilidade de receber conexão de um host que ainda não foi identificado. Por exemplo um host que acabou de se conectar, recebeu multicast de outros mas os outros ainda não receberam seu multicast. Se for este o caso, é interessante identificar o host que se conectou e trocar vistas parciais das listas de hosts. Já existe o escopo para implementar essa lógica:

            //ProtocolServer.groovy
            Host identifyClient(Socket client) {
                Host host = HostManager.findHostByAddress(client.inetAddress.hostAddress)
                ...
                if (!host) {
                    //TODO: identificar host conectado e trocar vistas parciais
                }
                return host
            }

        5.3.2) Aleatoriedade 

            Por enquanto, são gerados apenas índices aleatórios das listas de hosts ativos e informações novas, que são utilizados para selecionar tais objetos. O seed do Random é alimentado com o ID da thread. 
            
            //Protocol.groovy; seleção aleatória de Host para envio de mensagem através do ProtocolClient
            Random hostRandom = new Random(Thread.currentThread().id)
            Host host
            ...
            hostIndex = hostRandom.nextInt(HostManager.activeHostsCount)
            host = HostManager.activeHosts.get(hostIndex)
            ...

            Como melhorar aleatoriedade? Topologia?

        5.3.3) Topologia

            É interessante gerar sobreposições da rede (overlays). Seria possível organizar a topologia com diferentes critérios, algumas propostas:
                a) tempo de vida na rede
                b) número de informações
                c) número de informações criadas
                d) número de informações criadas num período de tempo
                e) número de informações conhecidas em comum
                f) vistas parciais
                g) estados (do Gossip Protocol) dos nós 
                h) estados 'isOnline' dos nós

            Quais estruturas utilizar para representar a topologia? Propostas: 
                - árvore binária para a), b), c) e d)
                - estrela para e)
                - grafos genéricos para f), g) e h)

            Críticas? Sugestões?

            Como implementar? Consulta dos nós?

        5.3.4) Consulta

            É interessante poder consultar outros nós da rede, além de propagar informações.
            
            Como implementar? 

            Proposta: Multicast.
                Através de multicast, enviar a consulta. Respondem aqueles que souberem a resposta.
                Necessário implementar um mecanismo para identificar a pergunta e as respostas associadas.
                Vantagem: todos ficam sabendo da pergunta e das respostas. 
                Desvantagem: menor confiabilidade de obter resposta precisa.

            Proposta: TCP/IP.
                Perguntar sequencialmente para os nós ativos. 
                Vantagem: maior confiabilidade da resposta.
                Desvantagem: apenas o nó que perguntou fica sabendo da resposta, que pode ser interessante aos demais nós. É mais custoso.

            Híbrido: TCP/IP e Multicast
                Perguntar sequencialmente para os nós ativos. 
                Divulgar a pergunta e resposta final via multicast.

            Sugestões de consulta: as mesmas da topologia. 
            Aparentemente, a topologia seria uma resposta final da consulta.

            Críticas? Sugestões?

6- Resultados

    Como foi listado em 5.2.7), ainda não foi implementado um mecanismo para imprimir em arquivo resultados da aplicação. Mas a aplicação funcionou no modo automático durante uma madrugada inteira com 4 computadores conectados na rede, conforme o esperado :)

    Críticas? Sugestões?
